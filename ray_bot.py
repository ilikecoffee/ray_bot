
PROMPT = """This is conversation between Ray Dalio and a manager."""

QUESTIONS = [
    "What is principle 10?",
    "Am I being too distant?",
    "What should I do if I think my report is going down a bad path?",
    "But isn’t this micro-managing?",
    "I feel like I’m micro-managing, what should I do",
    ]


def evaluate(model):
    for q in QUESTIONS:
        resp = model.get_resp(q)
        print('manager: {}'.format(q))
        print('ray: {}'.format(resp))

if __name__ == '__main__': 
    model = GooseGPTJ
    evaluate(model)

